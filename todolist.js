onload = todoListMain;

function todoListMain() {
  let inputElemitem,
    inputElemCategory,
    addbutton,
    selElem;

  getElements();
  addListeners();


  function getElements() {
    inputElemitem = document.getElementsByTagName("input")[0];
    inputElemCategory = document.getElementsByTagName("input")[1];
    addbutton = document.getElementById("addBtn");
    selElem = document.getElementById("catFilter");
  }

  function addListeners() {
    addbutton.addEventListener("click", addEntryItems, false);
    selElem.addEventListener("change", filterEntries, false);
  }

  function addEntryItems(event) {
    if (inputElemitem.value === "" || inputElemCategory.value === "") {
      alert("Please Enter Item and Slect Category name.")
    }
    else {
      let table = document.getElementById("itemdelete");
      
      let trElem = document.createElement("tr");
      trElem.className="a";
      table.appendChild(trElem);

      // checkbox 
      let checkboxElem = document.createElement("input");
      checkboxElem.type = "checkbox";
      checkboxElem.addEventListener("click", done, false);
      let tdElem1 = document.createElement("td");
      tdElem1.appendChild(checkboxElem);
      trElem.appendChild(tdElem1);

      // to-do 
      let tdElem2 = document.createElement("td");
      tdElem2.innerText = inputElemitem.value;
      trElem.appendChild(tdElem2);

      // category 
      let tdElem3 = document.createElement("td");
      tdElem3.innerText = inputElemCategory.value;
      trElem.appendChild(tdElem3);

      // delete 
      let spanElem = document.createElement("span");
      spanElem.innerText = "delete";
      spanElem.className = "material-icons";
      spanElem.addEventListener("click", deleteItem, false);
      let tdElem4 = document.createElement("td");
      tdElem4.appendChild(spanElem);
      trElem.appendChild(tdElem4);


      //remove items from list
      function deleteItem() {
        trElem.remove();
      }



      //strike line items
      function done() {
        trElem.classList.toggle("strike");
      }

      inputElemitem.value = "";
      inputElemCategory.value = "";

      let deleteList = document.getElementById("deleteList");
      deleteList.addEventListener("click", deallitems, false);

    
      function deallitems() {
        table.remove();

        let  bo,tr1,th1,th2,th3,th4;
        bo=document.getElementsByTagName("div")[0];
        table=document.createElement("table");
        table.className="List";
        table.width="50%";
        table.id="itemdelete";
        table.cellpadding="5";
        bo.appendChild(table);

        tr1=document.createElement("tr");
        table.appendChild(tr1);

        th1=document.createElement("th");
        th1.width="10%";
        table.appendChild(th1);

        th2=document.createElement("th");
        th2.width="20%";
        table.appendChild(th2);

        th3=document.createElement("th");
        th3.width="15%";
        table.appendChild(th3);

        th4=document.createElement("th");
        th4.width="10%";
        table.appendChild(th4);

      }

    }   

  } 

  function filterEntries() {
    let rows = document.getElementsByTagName("tr");

     for(let i=1; i<rows.length; i++)
    {
     // console.log(rows[i]);
     let category = rows[i].getElementsByTagName("td")[2].innerText;
    // let category = rows[i].getElementById("catFilter").innerText;
     if(category == selElem.value){
      rows[i].style.display = "";
     }else{
        rows[i].style.display = "none";
     }
    }

    // let selection = selElem.value;
    // if (selection == "") {
    //   //console.log("filterEntries running"); 
    //   let rows = document.getElementsByTagName("tr");
    //   //using for-each loop with Array.from
    //   Array.from(rows).forEach((row, index) => {
    //     row.style.display = "";
    //   });
    // }
    // else {
    //   //console.log("filterEntries running"); 
    //   let rows = document.getElementsByTagName("tr");
    //   //using for-each loop with Array.from
    //   Array.from(rows).forEach((row, index) => {
    //     if (index == 0) {
    //       return;
    //     }
    //     let category = row.getElementsByTagName("td")[2].innerText;
    //     if (category == selElem.value) {
    //       row.style.display = "";
    //     } else {
    //       row.style.display = "none";
    //     }

    //   });
    // }
  }

}

 //without for-each
    // for(let i=1; i<rows.length; i++)
    // {
    //  // console.log(rows[i]);
    //  let category = rows[i].getElementsByTagName("td")[2].innerText;
    //  if(category == selElem.value){
    //   rows[i].style.display = "";
    //  }else{
    //     rows[i].style.display = "none";
    //  }
    // }
